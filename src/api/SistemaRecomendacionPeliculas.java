package api;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.MergeSort;
import model.data_structures.MergeSort2_CompareTo;
import model.data_structures.NodoSencillo;
import model.vo.VOAgnoPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

import java.awt.List;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	private ILista<VORating> ratings;

	private ILista<VOGeneroPelicula> generoPelicula;

	private ILista<VOUsuarioConteo> usuarioConteo;

	private ILista<VOPeliculaUsuario> peliculaUsuario;

	private ILista<VOTag> tags;

	public final static String SEPARADOR = ",";



	public ISistemaRecomendacionPeliculas crearSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		try
		{
			misPeliculas = new ListaEncadenada<VOPelicula>();
			peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
			String temp[] = new String[3];
			int agno;
			generoPelicula = new ListaEncadenada<VOGeneroPelicula>();

			File archivo = new File(rutaPeliculas);
			BufferedReader in = new BufferedReader(new FileReader(archivo));
			String linea = in.readLine();
			while((linea = in.readLine()) != null)
			{
				if (linea.contains("\"")) 
				{
					temp[0] =linea.substring(0, linea.indexOf(','));
					temp[1] = linea.substring(linea.indexOf(',') + 2, linea.lastIndexOf(',') - 1);
					temp[2] = linea.substring(linea.lastIndexOf(',') + 1);

				} 
				else 
				{
					temp = linea.split(",");
				}
				try 
				{
					agno = Integer.parseInt(temp[1].substring(temp[1].lastIndexOf('(')+1, temp[1].lastIndexOf(')')));
				} 
				catch (NumberFormatException | StringIndexOutOfBoundsException e) 
				{
					if (temp[1].endsWith(")")) 
					{
						agno = Integer.valueOf(temp[1].substring(temp[1].lastIndexOf('(') + 1, temp[1].lastIndexOf('-')));
					} 
					else 
						agno = 0;
				}
				VOPelicula pelicula = new VOPelicula();
				pelicula.setIdPelicula(Long.parseLong(temp[0]));
				pelicula.setAgnoPublicacion(agno);
				misPeliculas.agregarElementoFinal(pelicula);
				String[] partesGeneros = temp[2].split("\\|");
				ILista<String> geneross = new ListaEncadenada<String>();
				int i=0;
				for( String p: partesGeneros)
				{
					boolean encontro = false;
					VOGeneroPelicula ng = new VOGeneroPelicula();
					ng.setGenero(p);
					generoPelicula.agregarElementoFinal(ng);
					geneross.agregarElementoFinal(p);

					for( int k = 0; k < generoPelicula.darNumeroElementos() && !encontro; k++)
					{

						generoPelicula.darElemento(k).setPeliculas(new ListaDobleEncadenada<VOPelicula>());
						VOGeneroPelicula gen = generoPelicula.darElemento(k);
						if( gen.getGenero().equals(p))
						{
							ILista<VOPelicula> listaP = gen.getPeliculas();
							listaP.agregarElementoFinal(pelicula);
							encontro = true;
							generoPelicula.darElemento(k).setPeliculas(listaP);
						}

					}
					if( encontro == false)
					{
						VOGeneroPelicula nuevoGen = new VOGeneroPelicula();
						ILista<VOPelicula> g = new ListaEncadenada<VOPelicula>();
						g.agregarElementoFinal(pelicula);
						nuevoGen.setPeliculas(g);
						nuevoGen.setGenero(p);
						generoPelicula.agregarElementoFinal(nuevoGen);

					}

				}
				pelicula.setGenerosAsociados(geneross);


				if( temp[1].endsWith(")"))
				{
					temp[1] = temp[1].substring(0, temp[1].lastIndexOf('(') - 1);
				}

				System.out.println(i + ": " + temp[1] + "(" + agno + ")" + " - " + generoPelicula.darElemento(0)  );


				pelicula.setTitulo(temp[1]);


			}

			VOAgnoPelicula anio;
			for( int j = 1950; j <= 2016; j++ )
			{
				anio = new VOAgnoPelicula();
				anio.setAgno(j);
				ListaEncadenada<VOPelicula> lista = new ListaEncadenada<>();
				for( int k = 0; k < misPeliculas.darNumeroElementos(); k++ )
				{

					if( misPeliculas.darElemento(k).getAgnoPublicacion() == j )
					{
						lista.agregarElementoFinal(misPeliculas.darElemento(k));
					}
				}
				anio.setPeliculas(lista);
				peliculasAgno.agregarElementoFinal(anio);
			}
			in.close();
			return true;
		}
		catch( IOException e )
		{

		}
		return false;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		try
		{
			ratings = new ListaEncadenada<VORating>();
			String temp[] = new String[3];

			File archivo = new File(rutaRatings);
			BufferedReader in = new BufferedReader(new FileReader(archivo));
			String linea = in.readLine();
			temp = linea.split(",");
			VORating rating = new VORating();
			while((linea = in.readLine()) != null)
			{
				temp[0] =linea.substring(0, linea.indexOf(','));
				temp[1] = linea.substring(linea.indexOf(',') + 2, linea.lastIndexOf(',') - 1);
				temp[2] = linea.substring(linea.lastIndexOf(',') + 1);
				
				rating.setIdUsuario(Long.parseLong(temp[0]));
				rating.setIdPelicula(Long.parseLong(temp[1]));
				rating.setRating(Double.parseDouble(temp[2]));

				ratings.agregarElementoFinal(rating);
				for( int i = 0; i < misPeliculas.darNumeroElementos(); i++)
				{
					if( misPeliculas.darElemento(i).getIdPelicula() == rating.getIdPelicula())
					{
						int n = 0;
						misPeliculas.darElemento(i).setNumeroRatings(n+1);
						double promRating;
						double rat = misPeliculas.darElemento(i).getPromedioRatings();
						promRating = ((rat * misPeliculas.darElemento(i).getNumeroRatings()) + rating.getRating()) / misPeliculas.darElemento(i).getNumeroRatings(); 
						misPeliculas.darElemento(i).setPromedioRatings(promRating);
					}
				}
			}

			in.close();
			return true;
		}

		catch( IOException e)
		{

		}

		return false;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		try
		{
			tags = new ListaEncadenada<VOTag>();
			String temp[] = new String[3];

			File archivo = new File(rutaTags);
			BufferedReader in = new BufferedReader(new FileReader(archivo));
			String linea = in.readLine();
			temp = linea.split(",");
			VOTag tag = new VOTag();
			while((linea = in.readLine()) != null)
			{
				temp[0] = linea.substring(0, linea.indexOf(','));
				temp[1] = linea.substring(linea.indexOf(',') + 2, linea.lastIndexOf(',') - 1);
				temp[2] = linea.substring(linea.lastIndexOf(',') + 1);
				temp[3] = linea.substring(linea.lastIndexOf(','), linea.lastIndexOf(' '));
				
				tag.setTag(temp[2]);
				tag.setTimestamp(Long.parseLong(temp[3]));
				tags.agregarElementoFinal(tag);
				for( int i = 0; i < misPeliculas.darNumeroElementos(); i++)
				{
					if(misPeliculas.darElemento(i).getIdPelicula() == Long.parseLong(temp[1]))
					{
						misPeliculas.darElemento(i).setTagsAsociados(tags.darElemento(i).getTag());
						
					}
				}	

			}
			in.close();
			return true;
		}
		catch( IOException e)
		{

		}
		return false;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return misPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> ordenada = null;
		int cont=0;
		VOPelicula actual= misPeliculas.darElemento(0);

		for (int i=0;i<misPeliculas.darNumeroElementos();i++)
		{
			if (actual.getTitulo().contains(busqueda))
			{
				ordenada.agregarElementoFinal(actual);
			}
			cont=cont+1;
			actual=misPeliculas.darElemento(cont);
		}
		return ordenada;
	}


	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> lista = new ListaDobleEncadenada<>();
		VOAgnoPelicula actual = peliculasAgno.darElemento(0);
		int contador = 0;

		for( int i = 0; i < peliculasAgno.darNumeroElementos(); i++)
		{
			if( actual.getAgno() == agno )
			{
				lista = actual.getPeliculas();
			}
			contador++;
			actual = peliculasAgno.darElemento(contador);
		}
		return lista;
	}


	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub

		ILista<VOGeneroPelicula> respuesta = new ListaEncadenada<VOGeneroPelicula>();

		for (int i=0; i < generoPelicula.darNumeroElementos(); i++)
		{
			ListaEncadenada<VOPelicula> peli = (ListaEncadenada<VOPelicula>) generoPelicula.darElemento(i).getPeliculas();
			MergeSort<VOPelicula> sort = new MergeSort<>();
			peli.setCabeza(sort.merge_sort(peli.darCabeza(), 1));
			VOGeneroPelicula ng = new VOGeneroPelicula();
			ng.setGenero(generoPelicula.darElemento(i).getGenero());
			ListaEncadenada<VOPelicula> p = new ListaEncadenada<>();
			for( int j = 0; j < n; j++)
			{
				p.agregarElementoFinal(peli.darElemento(j));
			}
			ng.setPeliculas(p);
			respuesta.agregarElementoFinal(ng);
		}
		for (VOGeneroPelicula genero: respuesta) {
			for (VOPelicula pelicula: genero.getPeliculas()) {
				System.out.println(pelicula.getIdPelicula()+" \t\t\tRating: " + pelicula.getPromedioRatings());
			}
		}

		return respuesta;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		ILista<VOPelicula> respuesta = new ListaDobleEncadenada<VOPelicula>();
		MergeSort2_CompareTo<VOPelicula> sort = new MergeSort2_CompareTo<VOPelicula>();
		NodoSencillo<VOPelicula> ordenado = sort.merge_sort(new NodoSencillo<VOPelicula>(misPeliculas.darElemento(0)) );
		while(ordenado != null){
			respuesta.agregarElementoFinal(ordenado.darElemento());
			ordenado = ordenado.darSiguiente();
		}
		return respuesta;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		ILista<VOGeneroPelicula> respuesta = new ListaEncadenada<VOGeneroPelicula>();

		for (int i=0; i < generoPelicula.darNumeroElementos(); i++)
		{
			ListaEncadenada<VOPelicula> peli = (ListaEncadenada<VOPelicula>) generoPelicula.darElemento(i).getPeliculas();
			MergeSort<VOPelicula> sort = new MergeSort<>();
			peli.setCabeza(sort.merge_sort(peli.darCabeza(), 1));
			VOGeneroPelicula ng = new VOGeneroPelicula();
			ng.setGenero(generoPelicula.darElemento(i).getGenero());
			ng.setPeliculas((ILista<VOPelicula>) peli.darElemento(0));
			respuesta.agregarElementoFinal(ng);
		}

		return respuesta;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		ILista<VOGeneroPelicula> respuesta = new ListaEncadenada<VOGeneroPelicula>();

		for (int i=0; i < generoPelicula.darNumeroElementos(); i++)
		{
			ListaEncadenada<VOPelicula> peli = (ListaEncadenada<VOPelicula>) generoPelicula.darElemento(i).getPeliculas();
			MergeSort<VOPelicula> sort = new MergeSort<>();
			peli.setCabeza(sort.merge_sort(peli.darCabeza(), 2));
			VOGeneroPelicula ng = new VOGeneroPelicula();
			ng.setGenero(generoPelicula.darElemento(i).getGenero());
			ng.setPeliculas((ILista<VOPelicula>) peli.darElemento(0));
			peli.darElemento(i).getTagsAsociados();
			respuesta.agregarElementoFinal(ng);
		}
		
		return respuesta;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		ILista<VORating> lr = new ListaEncadenada<VORating>();
		for( int i = 0; i < ratings.darNumeroElementos(); i++ )
		{
			if( ratings.darElemento(i).getIdPelicula() == idPelicula)
			{
				lr.agregarElementoFinal(ratings.darElemento(i));
			}
		}
		MergeSort<VORating> sort = new MergeSort<>();
		((ListaEncadenada<VORating>) lr).setCabeza(sort.merge_sort(((ListaEncadenada<VORating>) lr).darCabeza(), 1));
		
		
		return lr;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarOperacionSR(String idOperacion, long tInicio, long tFin) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darHistorialOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	@Override
	public int agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}




}
