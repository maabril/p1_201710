package view;

import api.SistemaRecomendacionPeliculas;

public class vista {
	public static void main(String args[]) {
		SistemaRecomendacionPeliculas srp =  new SistemaRecomendacionPeliculas();
		srp.cargarPeliculasSR("data/movies.csv");
		srp.cargarRatingsSR("data/ratings.csv");
		srp.peliculasPopularesSR(5);
	}
}
