package model.data_structures;

import model.vo.VOPelicula;

public interface IComparator<T> {

	public double comparar(T a1, int criterio);

	int compareTo(T objeto);
}
