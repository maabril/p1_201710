package model.data_structures;

public class NodoDoble <T>{

	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	private T objeto;
	
	public NodoDoble ( T pObjeto)
	{
		siguiente=null;
		anterior=null;
		objeto=pObjeto;
	}
	
	public void agregarSiguiente(NodoDoble<T> pSiguiente)
	{
		siguiente=pSiguiente;
	}
	public NodoDoble darSiguiente()
	{
		return siguiente;
	}
	public void agregarAnterior(NodoDoble<T> pAnterior)
	{
		anterior=pAnterior;
	}
	public NodoDoble darAnterior()
	{
		return  anterior;
	}
	public T darElemento()
	{
		return objeto;
	}
	
}
