package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {
	NodoSencillo<T> actual;
	NodoSencillo<T> primero;
	NodoSencillo<T> siguiente;
	int num = 0;

	@Override
	public Iterator<T> iterator() {
		class iterador<T> implements Iterator{
			NodoSencillo<T> a;


			iterador(NodoSencillo<T> p)
			{
				a =p;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(a==null)
					return false;
				else
					return true;
			}

			@Override
			public Object next() {
				// TODO Auto-generated method stub
				if (this.hasNext()==true)
				{
					NodoSencillo<T> temporal;
					temporal=a;
					a=a.darSiguiente();
					return temporal;
				}
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub


			}
		}
		return null;
	}

	public void agregarElementoFinal(T elem) {
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		if( primero == null )
		{
			primero = nuevo;
		}
		else
		{
			NodoSencillo<T> nod = primero;
			while(nod.darSiguiente()!=null){
				nod = nod.darSiguiente();
			}
			nod.agregarSiguiente(nuevo);
		}
		num++;

	}

	public NodoSencillo<T> darCabeza() {
		return primero;
	}
	
	public void setCabeza(NodoSencillo<T> cabeza) {
		primero = cabeza;
	}
	
	public T darElemento(int pos) {
		int cont=0;
		NodoSencillo<T> nod = primero;
		if(pos==0)
		{
			return nod.darElemento();
		}
		else
		{


			while(nod.darSiguiente()!=null){
				nod = nod.darSiguiente();
				cont=cont+1;
				if (cont == pos)
				{
					return nod.darElemento();

				}
			}
		}
		return null;
	}

	@Override
	public int darNumeroElementos() {

		return num;

	}

	@Override
	public T eliminarElemento(int pos) {
		
		boolean encontro = false;
		if( primero != null  && pos == 0){
			primero = primero.darSiguiente();
			if( primero != null){
				encontro = true;
			}
		}
		else{
			NodoSencillo<T> actual = primero;
			int cont = 1;
			NodoSencillo<T> anterior = null;
			while( actual.darSiguiente()!= null && cont <= pos){
				anterior = actual;
				actual = actual.darSiguiente();
				if( cont == pos){
					encontro = true;
				}
				cont++;
			}
			if( actual.darSiguiente() == null ){
				anterior.agregarSiguiente(null);
			}
			else{
				anterior.agregarSiguiente(actual.darSiguiente());
			}

			actual.agregarSiguiente(null);
		}
		if( encontro){
			num--;
			return null;
		}
		return null;

	}
}
