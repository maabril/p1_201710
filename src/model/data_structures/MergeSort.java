package model.data_structures;

import java.util.Comparator;

public class MergeSort <T extends IComparator<T>>{

	public NodoSencillo<T> merge_sort(NodoSencillo<T> head, int criterio) 
	{
		if(head == null || head.darSiguiente() == null) { 
			return head; 
		}
		NodoSencillo<T> middle = getMiddle(head);      
		NodoSencillo<T> sHalf = middle.darSiguiente(); 
		middle.agregarSiguiente(null);   

		return merge(merge_sort(head, criterio),merge_sort(sHalf, criterio), criterio); 
	}

	public NodoSencillo<T> merge(NodoSencillo<T> a, NodoSencillo<T> b, int criterio) 
	{
		NodoSencillo<T> dummyHead;
		NodoSencillo<T> curr; 
		dummyHead = new NodoSencillo<T>(); 
		curr = dummyHead;
		while(a !=null && b!= null) 
		{
			if(a.darElemento().comparar(b.darElemento(), criterio) <= 0) { 
				curr.agregarSiguiente(a); a = a.darSiguiente(); 
			}
			else 
			{ 
				curr.agregarSiguiente(b); b = b.darSiguiente(); 
			}
			curr = curr.darSiguiente();
		}
		curr.agregarSiguiente((a == null) ? b : a);
		return dummyHead.darSiguiente();
	}

	public NodoSencillo<T> getMiddle(NodoSencillo<T> head) {
		if(head == null) 
		{ 
			return head; 
		}
		NodoSencillo<T> slow;
		NodoSencillo<T> fast; 
		slow = fast = head;
		while(fast.darSiguiente() != null && fast.darSiguiente().darSiguiente() != null) 
		{
			slow = slow.darSiguiente(); 
			fast = fast.darSiguiente().darSiguiente();
		}
		return slow;
	}





}