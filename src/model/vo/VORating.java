package model.vo;

import model.data_structures.IComparator;

public class VORating implements IComparator<VORating> {
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public void setTimestamp(long timest){
		this.timestamp = timest;
	}
	public long getTimestamp()
	{
		return timestamp;
	}
	@Override
	public double comparar(VORating a1, int criterio) {
		// TODO Auto-generated method stub
		if( criterio == 1)
		{
			return a1.getTimestamp() - this.timestamp;
		}
		return 0;
	}
	@Override
	public int compareTo(VORating objeto) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
