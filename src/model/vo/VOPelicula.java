package model.vo;

import model.data_structures.IComparator;
import model.data_structures.ILista;

public class VOPelicula implements IComparator<VOPelicula>{

	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;

	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;



	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	@Override
	public double comparar(VOPelicula a1, int criterio) {
		// TODO Auto-generated method stub
		if( criterio == 0)
		{
			return this.promedioRatings - a1.getPromedioRatings();
		}
		else if( criterio == 1)
		{
			return a1.getPromedioRatings() - this.promedioRatings;
		}
		else if( criterio == 2)
		{
			return this.agnoPublicacion - a1.getAgnoPublicacion();
		}
		return 0;
	}

	@Override
	public int compareTo(VOPelicula pelicula){
		if(this.agnoPublicacion > pelicula.getAgnoPublicacion())
			return 1;
		else if(this.agnoPublicacion < pelicula.getAgnoPublicacion())
			return -1;
		if(this.getPromedioRatings() > pelicula.getPromedioRatings())
			return 1;
		else if(this.getPromedioRatings() < pelicula.getPromedioRatings())
			return -1;
		return this.getTitulo().compareTo(pelicula.getTitulo());

	}



}
